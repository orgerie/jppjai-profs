---
title: "Atelier portraits"
---


- [Exposé](Sensibilisation-stereotypes.pptx)
- [Explications sommaires de l'exposé](deroulement.pdf)
- [Biographies succinctes des 10 portraits](bios.pdf)
- [Matériel individuel pour réaliser la partie qualificatifs des informaticiens et informaticiennes](qualificatifs.pdf) 
- [Matériel individuel pour réaliser la partie portraits](planche.pdf)
- [Matériel pour la partie portraits et qui-fait-quoi au tableau](gdplanches.pdf)
- [Solution de la partie qui-fait-quoi (avec timeline)](quifaitquoi-solution.pdf)

