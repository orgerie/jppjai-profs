---
title: "Exposé orientation"
---


## Ressources
- Les [slides](JPPJAI-2024-orientation.pdf) de l'exposé

### Kit d'orientation complet
- [Kit d'orientation](https://msha.ke/lesdecodeuses.orientation), plusieurs ressources pour s'orienter en collège, voie professionnelle et lycée vers les métiers du numérique



### Description des métiers 
- [Les métiers des mathématiques, de la statistique et de l'informatique](https://www.onisep.fr/Decouvrir-les-metiers/Des-metiers-qui-recrutent/La-collection-Zoom-sur-les-metiers/Les-metiers-des-mathematiques-de-la-statistique-et-de-l-informatique), guide ONISEP (gratuit en ligne)
- [Description des secteurs d'application du numérique](https://kitpedagogique.onisep.fr/metiersdunumerique/Videos-secteurs), vidéos sur le site de l'ONISEP 
- [Les métiers du numérique](https://www.onisep.fr/Publications/Parcours/Les-metiers-du-numerique), guide ONISEP (payant)
- [Fiches métiers du numérique](https://www.interface3namur.be/orientation/fiches-metiers/) réalisées par Interface3Namur  (avec les cursus possibles en Belgique)


### Choix des filières
- [Comment choisir ses spécialités en première et terminale](https://talentsdunumerique.com/trouver-ma-formation/specialites-choisir)
- [Quel bac choisir](https://talentsdunumerique.com/trouver-ma-formation/bac-choisir)



### Exemples de portraits vidéos 
- [Juan Cortés](https://www.youtube.com/watch?v=BaOQ_LfRYao), algorithmicien-roboticien  
- [Louise Travé-Massuyès](https://www.youtube.com/watch?v=97rKOdjtI58&list=PL_nuutQCh3LAlvc4iyTFcBHMJCwlj4QES&index=45), automaticienne 
- [Georges Da Costa](https://www.youtube.com/watch?v=2gISxQ3b_1g&list=PL_nuutQCh3LAlvc4iyTFcBHMJCwlj4QES&index=39&ab_channel=ScienceAnimation), informaticien 
- [Marielle Simon](https://kitpedagogique.onisep.fr/metiersdunumerique/Videos-selfie/Marielle-chercheuse-en-mathematiques), mathématicienne 
- [Série Qui cherche... cherche](https://www.youtube.com/playlist?list=PL_nuutQCh3LAlvc4iyTFcBHMJCwlj4QES)


