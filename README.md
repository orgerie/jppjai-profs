---
title: "J'Peux Pas, J'Ai Informatique  (aka JPPJAI)"
---



## Historique de la Journée J'peux pas, j'ai informatique (aka JPPJAI)
Partant du constat qu'il y a peu de diversité dans les candidatures de stages d'observation de 3ème que nous recevons au laboratoire, nous avons construit une journée pour sensibiliser les élèves de 5ème à la très grande diversité de l'informatique et encourager les jeunes filles à s'y intéresser. La première édition a eu lieu en avril 2018. Depuis 2021, cette journée est destinée aux enseignants et enseignantes de collèges et lycées. Les précédentes éditions :   
- 10 novembre 2021   
- 9 novembre 2022   
- 7 février 2024  
- 29 janvier 2025    

## Objectif : déconstruire les stéréotypes liés à l'informatique
Cette journée s'articule autour de 3 ateliers de déconstruction d'idées reçues :   
- Les informaticiens et informaticiennes ne sont pas que des geeks.   
- L'informatique, ce n'est pas que des ordinateurs.   
- Coder, ça ne sert pas que à concevoir des jeux vidéo.   


## Sur ce site : les ressources utilisées pendant la journée de 2025
Ce site regroupe les ressources présentées dans les différents exposés et ateliers de la journée :   
- [exposé d'introduction](introduction/) (contexte, enjeux et chiffres actuels)   
- [atelier portraits](portraits/) : sur des informaticiens et informaticiennes célèbres et pas forcément geeks   
- [atelier informatique débranchée](informatique_débranchée/) : sur des notions clés de l’informatique sans ordinateur   
- [atelier bases de données](bases_de_données/) :  sur les stéréotypes et la diversité des profils et des domaines de l’informatique autres que les jeux vidéos  
- [exposé orientation](orientation/) (métiers du numérique)   
- [exposé démos](démos/) (recherches en informatique)   

Nous mettons à disposition ces ressources principalement pour qu'elles soient réutilisées dans les collèges et lycées. N'hésitez pas à nous dire si vous les utilisez, vos retours d'expériences nous intéressent !

## Kit d'orientation complet
- [Kit d'orientation](https://msha.ke/lesdecodeuses.orientation), plusieurs ressources pour s'orienter en collège, voie professionnelle et lycée vers les métiers du numérique

## D'autres actions 
- [L codent, L créent à Rennes](https://lclc-rennes.irisa.fr)
    - l'intégralité des contenus des ateliers est disponible [ici](https://semdyal.trinket.io/lclc#/introduction/les-images-et-processing)
    - les oeuvres réalisées l'an dernier sont visibles [ici](https://lclc-rennes.irisa.fr/les-oeuvres-finales/)
- [L codent, L créent à Brest](https://www.ensta-bretagne.fr/fr/l-codent-l-creent)
- [L codent, L créent à Lille](https://informatique.univ-lille.fr/lclc/)
    - les [réalisations](https://wikis.univ-lille.fr/chticode/wiki/infogirl/accueil) des différentes années
    - la [page avec le contenu des séances](https://wikis.univ-lille.fr/chticode/wiki/ecoles/lclc/doc/home)
    - une [description](https://hal.univ-brest.fr/hal-02911731/document) des ressources développées dans ce contexte
