---
title: "Atelier base de données (BD)"
---



## Présentation de l'atelier
- Les [slides](JPPJAI-2025-BD.pdf)

## Ressources
- [Why can't girls code](https://www.youtube.com/watch?v=LVwOWQQ4pCw), une vidéo humoristique en anglais sur les raisons qui empêcheraient les filles de coder (niveau lycée)
- [Kit d'orientation](https://msha.ke/lesdecodeuses.orientation), plusieurs ressources pour s'orienter en collège, voie professionnelle et lycée vers les métiers du numérique
- [Les métiers du numérique](https://oniseptv.onisep.fr/video/les-metiers-du-numerique), une vidéo de l'ONISEP
- [Guide ONISEP : les métiers du numérique](https://www.onisep.fr/publications/parcours/publication-les-metiers-du-numerique)
- [Zoom ONISEP : mathématiques, statistique et informatique](https://www.onisep.fr/metier/des-metiers-qui-recrutent/la-collection-zoom-sur-les-metiers/publication-les-metiers-des-mathematiques-statistique-et-informatique), le pdf est téléchargeable librement
- [Vidéo exemple des trophées NSI](https://tube-sciences-technologies.apps.education.fr/w/xhXAf72Cr4HMnZJV9rzunw)
- [Site web d'orientation pour les métiers du numérique](https://talentsdunumerique.com/trouver-ma-formation/bac-choisir), focus sur quel bac choisir
- [Vidéo d'une ingénieure-chercheuse en intelligence artificielle](https://oniseptv.onisep.fr/video/ingenieure-chercheure-en-intelligence-artificielle-2)
- [Vidéo sur un historique filles et numérique](https://oniseptv.onisep.fr/video/filles-et-numerique-historique)
- Bande dessinée [Les Décodeuses du numérique](https://www.ins2i.cnrs.fr/les-decodeuses-du-numerique)
- Fiches pédagogiques associées
   - [Vidéo sur le ressenti d'une décodeuse](https://www.youtube.com/watch?v=tf1w4tkYeXA)
   - [Vidéo sur les décodeuses en action de médiation](https://www.youtube.com/watch?v=XI1u9UPd21o)
   - [Les données structurées et leur traitement](https://www.ins2i.cnrs.fr/sites/institut_ins2i/files/page/2021-09/donnees_structurees_traitement.pdf)
   - [Internet, Web & réseaux sociaux](https://www.ins2i.cnrs.fr/sites/institut_ins2i/files/page/2021-09/internet_web_reseaux_sociaux.pdf)
   - [Le livret pégagogique complet](https://www.ins2i.cnrs.fr/sites/institut_ins2i/files/news/2022-10/LivretPeda_13-10_last.pdf)
- [Rendez-vous des jeunes mathématiciennes et informaticiennes](https://filles-et-maths.fr/rjmi/)
- [Informations sur les stages, les formations et les ressources concernant l'Intelligence Artificielle](https://intelligence-artificielle.univ-rennes.fr/)
