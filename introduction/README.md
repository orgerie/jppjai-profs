---
title: "Exposé d'introduction"
---


## Ressources
- Les [slides](JPPJAIprofs2025-intro.pdf) de l'exposé d'introduction
- Le [quizz](quizz_JPPJAI2021_eleves.pdf) envoyé au préalable de la journée pour les élèves et présenté dans l'explosé d'introduction
- Les [slides](JPPJAIprofs2025-restitution.pdf) de la restitution à la fin de la journée

## Sources de l'exposé
- livret réalisé par [Interface3Namur](https://www.interface3namur.be) pour déstigmatiser les filles dans les métiers de l'informatique (niveau collège) :  [Carnet pratique pour... plus de mixité dans les métiers informatiques](https://www.interface3namur.be/documentation/carnet-mixite-metiers-informatiques/)
- chiffres sur le nombre de femmes parmi dans les corps des maîtres de conférences et professeurs des universités suivant les disciplines : [fiches démographiques des sections CNU 2022](https://www.enseignementsup-recherche.gouv.fr/sites/default/files/2023-10/section-27---informatique---2022-29622.pdf)
- part des femmes universitaires titulaires, insertion professionnelle des étudiants et étudiantes : [Vers l'égalité femmes-hommes ? Chiffres clés, MESR, 2024](https://www.enseignementsup-recherche.gouv.fr/sites/default/files/2024-03/vers-l-galit-femmes-hommes-chiffres-cl-s-2024-32097.pdf)
- mixité dans les entreprises bretonnes du secteur numérique : [Observatoire Régional des Compétences Numériques, enquête Bretagne 2024](https://orcn.fr/wp-content/uploads/2024/05/R8386-ORCN-2024-BZH-VF-3.pdf)
- part des femmes parmi les diplômés universitaires, taux de réussite au bac et taux de mentions, sentiment de réussite en français et mathématiques : [Filles et garçons, sur le chemin de l'égalité de l'école à l'enseignement supérieur, MEN, 2024](https://www.education.gouv.fr/filles-et-garcons-sur-le-chemin-de-l-egalite-de-l-ecole-l-enseignement-superieur-edition-2024-413799)
- parité dans les choix des enseignements de spécialité en 1ère et Terminale générales : [Les choix d’enseignements de spécialité et d’enseignements optionnels à la rentrée 2023, DEPP, 2024](https://www.education.gouv.fr/les-choix-d-enseignements-de-specialite-et-d-enseignements-optionnels-la-rentree-2023-413847)
- parité dans les niveaux en compétences informatiques en 3ème : [En fin de troisième, près de deux élèves sur trois ont une maîtrise satisfaisante des compétences numériques, DEPP, 2023](https://www.education.gouv.fr/en-fin-de-troisieme-pres-de-deux-eleves-sur-trois-ont-une-maitrise-satisfaisante-des-competences-379947)


## Vidéos de l'exposé
- [La science c'est pour toi](https://www.youtube.com/watch?v=ZhzPNE0-bQo), une vidéo sur les impacts liés au manque de diversité chez les scientifiques (niveau collège)
- [Qu'est-ce que l'informatique](https://www.youtube.com/watch?v=QJRoG9mC9Fg), une vidéo sur une courte définition de l'informatique (niveau collège)
- [Science it's a girl thing](https://www.youtube.com/watch?v=GMOqpxlW66E), un clip dégradant financé par la Commission Européenne en 2012


## Autres vidéos utiles
- [Qu'est ce qu'un algorithme](https://www.youtube.com/watch?v=tbmKIErjnns), une vidéo réalisée par educode pour expliquer les notions d'algorithme et de programme informatique
- [Comment les idées reçues changent-elles le cerveau des filles](https://www.francetvinfo.fr/societe/education/mathematiques-comment-les-idees-recues-changent-elles-le-cerveau-des-filles_1212967.html), une vidéo sur France Info sur la menace du stéréotype
- [Les hommes peuvent-ils devenir nuls en math](https://vimeo.com/395464058), une conférence d'Isabelle Régner, Professeure des Universités en psychologie sociale expérimentale à Aix-Marseille Université
- [La science a mauvais genre](https://mon-e-college.loiret.fr/POD/video/3383-la-science-a-mauvais-genre/), un documentaire de France Télévisions sur la place des femmes dans les métiers scientifiques et les liens avec l'enseignement
- [Picture a scientist](https://www.pictureascientist.com), un documentaire en anglais qui retrace le parcours de plusieurs chercheuses y compris les embûches    

