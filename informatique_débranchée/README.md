---
title: "Atelier Informatique débranchée"
---


## Activités présentées

### Le jeu de Nim avec de l'IA

- description détaillée de l'activité [ici](https://members.loria.fr/MDuflot/files/med/doc/Nim/ficheIAnim.pdf)
- description plus succincte [ici](https://members.loria.fr/MDuflot/files/med/IAnim.html)
- vidéo de l'activité [ici](https://www.dailymotion.com/video/x7magex?start=50) (de 1:00 à 21:30)
- une autre version de l'IA pour cette activité [ici](https://mmi-lyon.fr/?site_ressource_peda=jeu-de-nim-et-ia)

### Le crêpier psychorigide
<img src="crepier-image.png" width="300"/>

- Livret de présentation de l'activité [ici](crepier.pdf)
- Vidéo de présentation des règles [ici](https://www.youtube.com/watch?v=tI6uTAlX-_w)
- Matériel à imprimer et découper fourni par Martin Quinson [ici](algo1-supports.pdf)
- Article vulgarisé qui explique à quoi peut servir cet algorithme [là](https://interstices.info/genese-dun-algorithme/)
- Description de Marie Duflot-Kremer des règles du jeu [là](https://members.loria.fr/MDuflot/files/med/crepier.html)
- Fiche pédagogique [là](https://www.pedagogie.ac-nantes.fr/medias/fichier/notions-d-algorithmes-au-cycle-3-sequence-2-jeu-du-crepier_1544007245586-pdf)
- le pseudocode de l'algorithme :
```python
trier(tas_non_trié):
    while la pile est non triée
        insérer la spatule sous la plus grande crêpe non triée
        retourner la pile
        if la crêpe du dessus de la pile est côté coloré:
            insérer la spatule sous la crêpe du dessus de la pile
            retourner la pile
        insérer la spatule sous la crêpe qui était précédemment sur le dessus de la pile
        retourner la pile

```


## Autres Activités

- [Livret](https://github.com/InfoSansOrdi/CSIRL/blob/master/algo1/algo1-livret.pdf) qui décrit plusieurs activités débranchées ainsi que les concepts informatiques sous-jacents
- Traduction en français du best-seller mondial [Computer Science Unplugged](https://interstices.info/upload/csunplugged/CSUnplugged_fr.pdf)
- Page de [Martin Quinson](http://people.irisa.fr/Martin.Quinson/Mediation/SMN/)
- Page de [Marie Duflot-Kremer](https://members.loria.fr/MDuflot/files/med/index.html)
- Activités sur le site [Pixees](https://pixees.fr/category/support-pedagogique/activite/activite-debranchee/)
- Autres [activités](https://github.com/InfoSansOrdi/pedago-rennes) de Martin Quinson
